import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;


public class Login {

    @Test
    public void Login() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://login.sendpulse.com/login/");
        driver.findElement(By.cssSelector("#login")).clear();
        driver.findElement(By.cssSelector("#login")).sendKeys("super-test2017@ukr.net");
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#password")).sendKeys("20172017");
        driver.findElement(By.cssSelector(".btn.btn-green")).sendKeys(Keys.ENTER);
        driver.close();
    }

}

