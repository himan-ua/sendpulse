import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Upload {

    WebDriver driver = new FirefoxDriver();

    @Test
    public void EmailserviceAddressbooks() {
        //1. Enter Login Page
        driver.get("https://login.sendpulse.com/login/");
        //2. Input Login, Password and Click Login Button
        driver.findElement(By.cssSelector("#login")).clear();
        driver.findElement(By.cssSelector("#login")).sendKeys("super-test2017@ukr.net");
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#password")).sendKeys("20172017");
        driver.findElement(By.cssSelector(".btn.btn-green")).sendKeys(Keys.ENTER);
        //3. Click on Address book icon in Menu
        driver.findElement(By.cssSelector(".sp-icon.icon-address-book-2")).click();
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        //4. Click on Create new Address book Button
        driver.findElement(By.cssSelector(".btn.btn-create")).click();
        //5. Enter new Address book name and click OK to go to next screen
        driver.findElement(By.cssSelector("#add_new_addressbook_name")).clear();
        driver.findElement(By.cssSelector("#add_new_addressbook_name")).sendKeys("New Contact List 2");
        driver.findElement(By.cssSelector(".btn.btn-primary.add-ok")).click();
        //6. Click on Upload File Button
        driver.findElement(By.cssSelector("html body div#page-wrapper.site-wrapper.clearfix div#content-placeholder div#content-warpper div.width_limit_wide div#es-abook-upload-form div#upload_form_content_bookmark.width_limit_narrow div.bookmark-arrow span.label.label-as-link.label-yellow.arrow_box")).click();

        //7. Click "Browse" and specify file to be uploaded
        driver.findElement(By.cssSelector("#myfile")).sendKeys("C:\\Users\\adminn\\Documents\\Work\\SendPulse\\Email.xlsx");
        //8. Click ‘Submit’ button;
        driver.findElement(By.cssSelector("#submitbtn")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        //9. Click Continue Button to go to next page
        driver.findElement(By.cssSelector("#recipient-emails-upload")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        //10) Verify that input information is procesed and ‘Идет процесс добавления адресов в адресную книгу. Можете переходить на другую страницу - адреса загрузятся в фоновом режиме.’ text is visible;
        Assert.assertEquals(driver.findElement(By.cssSelector("#status-description")).getText(), "  Идет процесс добавления адресов в адресную книгу. Можете переходить на другую страницу - адреса загрузятся в фоновом режиме.");

        driver.close();
    }
}
