import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class Register {
    public WebDriver driver;
    public String baseUrl;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\adminn\\Documents\\Work\\Selenide\\chromedriver.exe");
        driver = new ChromeDriver();
        String baseUrl = "https://sendpulse.com/ru/register";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }


    @Test
    public void Register() {

        //1) Registering new customer
        driver.findElement(By.cssSelector("#inputName")).clear();
        driver.findElement(By.cssSelector("#inputName")).sendKeys("11111");
        driver.findElement(By.cssSelector("#inputEmail")).clear();
        driver.findElement(By.cssSelector("#inputEmail")).sendKeys("super-test2017@ukr.net");
        driver.findElement(By.cssSelector("#inputPhone")).clear();
        driver.findElement(By.cssSelector("#inputPhone")).sendKeys("1112233");
        driver.findElement(By.cssSelector("#inputPassword")).clear();
        driver.findElement(By.cssSelector("#inputPassword")).sendKeys("20172017");
        driver.findElement(By.cssSelector("#iAntiSpanRule")).isEnabled();
        //Click "Регистрация" button to call for validation of CAPTCHA and ask a user to manually input CAPTCHA
        driver.findElement(By.cssSelector("#btn-reg")).click();

        //Here the tester manually solves CAPTCHA
        driver.findElement(By.cssSelector("iframe[title='виджет reCAPTCHA']"));

        //Click "Регистрация" button  again to go to next screen
        driver.findElement(By.cssSelector("#btn-reg")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Enter Code recieved in Email
        //Ideally the code must be copied from the received email, however for simplicity i did not automate opening email box and finding the code in email text.
        driver.findElement(By.cssSelector("#regcode")).clear();
        driver.findElement(By.cssSelector("#regcode")).sendKeys("594133");
        //Click on "Напомнить позже" link
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#wizardClose")).click();
        //Make sure we are logged in and see the text "Сервис рассылок SendPulse"
        Assert.assertEquals(driver.findElement(By.cssSelector("#front-page-email-service")).getText(), "Сервис рассылок SendPulse");
        driver.close();
    }
}





