import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EmailserviceAddressbooks {
        WebDriver driver = new FirefoxDriver();


    @Test
    public void EmailserviceAddressbooks() {
        //1. Enter Logim Page
        driver.get("https://login.sendpulse.com/login/");
        //2. Input Login, Password and Click Login Button
        driver.findElement(By.cssSelector("#login")).clear();
        driver.findElement(By.cssSelector("#login")).sendKeys("super-test2017@ukr.net");
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#password")).sendKeys("20172017");
        driver.findElement(By.cssSelector(".btn.btn-green")).sendKeys(Keys.ENTER);
        //3. Click on Address book icon in Menu
        driver.findElement(By.cssSelector(".sp-icon.icon-address-book-2")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //4. Click on Create new Address book Button
        driver.findElement(By.cssSelector(".btn.btn-create")).click();
        //5. Enter new Address book name and click OK
        driver.findElement(By.cssSelector("#add_new_addressbook_name")).clear();
        driver.findElement(By.cssSelector("#add_new_addressbook_name")).sendKeys("New Contact List 1");
        driver.findElement(By.cssSelector(".btn.btn-primary.add-ok")).click();
        //6. Input manually 2 contacts
        driver.findElement(By.cssSelector("#emailsList")).sendKeys("super-test2017@ukr.net SendPulseTest\n" +
                "test2017@gmail.com Test");
        //7. Press Upload Button
        driver.findElement(By.cssSelector("#form_text input.btn")).click();
        //8. Click Continue Button to go to next page
        driver.findElement(By.cssSelector("#recipient-emails-upload")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        //9) Verify that input information is procesed and ‘Идет процесс добавления адресов в адресную книгу. Можете переходить на другую страницу - адреса загрузятся в фоновом режиме.’ text is visible;
        Assert.assertEquals(driver.findElement(By.cssSelector("#status-description")).getText(), "  Идет процесс добавления адресов в адресную книгу. Можете переходить на другую страницу - адреса загрузятся в фоновом режиме.");

        driver.close();

    }

}

